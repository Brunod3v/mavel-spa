import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CharactersComponent } from './views/characters/characters.component';
import { MainComponent } from './views/main/main.component';

const routes: Routes = [
      {
        path: '',
        component: MainComponent,
      },
      {
        path: 'characters',
        component: CharactersComponent,
      },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
