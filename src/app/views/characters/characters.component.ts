import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { CharactersService } from 'src/app/utils/services/characters.service';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.css']
})
export class CharactersComponent implements OnInit {

  objs: any;

  constructor(
    private service: CharactersService
  ) { }

  listAll!: Observable<any>;

  ngOnInit(): void {
    this.listAllCharacters()
  }

   async listAllCharacters(){
    await this.service.listAll().subscribe(data => {
      this.objs = data;
      console.log(this.objs)
    });
  }

}
