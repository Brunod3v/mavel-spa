import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import md5 from 'md5-ts';

@Injectable({
  providedIn: 'root'
})
export class CharactersService {
  urlController = 'https://gateway.marvel.com/v1/public/characters';
  publicKey = '82df67271fb227b96277ffc8c29ff4d5';
  privateKey = 'f522eeffe2c2b31ab974d9aa9a9b21e6cb50a861';
  dateTime = Number(new Date());

  private hash = md5(this.dateTime + this.privateKey + this.publicKey)

  constructor(private http: HttpClient) { }

  listAll(): Observable<any>{
    return this.http.get<any>(`${this.urlController}?ts=${this.dateTime}&apikey=${this.publicKey}&hash=${this.hash}`)
    .pipe(map((data: any) => data.data.results))
  }
}